


const ul = document.getElementById("fibonacci");

//CREACIÓN DEL ALGORITMO
function generarFibonacci(numerolimite){

    //Valores iniciales;
    const fib = [0,1]

    //creacion de un bucle
    for(let i = 2; i <numerolimite; i++){


        fib[i] = fib[i -1 ] + fib[i - 2];
    }

    return fib;
}

const data = parseInt(prompt("ingresa un número para cálcular la serie de Fibonacci"))


const fibSerie = generarFibonacci(data);

fibSerie.forEach(numero => {
    const li = document.createElement("li");
    li.innerHTML = numero;
    ul.append(li)
});