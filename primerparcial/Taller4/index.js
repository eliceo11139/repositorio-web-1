const form = document.getElementById("formulario");
const form2 = document.getElementById("formulario-2");
const form3 = document.getElementById("formulario-3");

const base = document.getElementById("base");
const altura = document.getElementById("altura");

const impar = document.getElementById("impar");

const texto = document.getElementById("texto");

const mostrar = document.getElementById("mostrar");
const mostrar2 = document.getElementById("mostrar-2");
const mostrar3 = document.getElementById("mostrar-3");

let Datos = [];

form.addEventListener("submit", (e) => {
  e.preventDefault();

  if (base.value) {
    const node = document.createElement("li");

    Datos.push(base.value, altura.value);

    let datosparse = Datos.splice(",");

    const basenode = document.createTextNode(
      "Base:" + datosparse[0] + ", "
    );
    const alturanode = document.createTextNode(
      "Altura:" + datosparse[1] + ", "
    );

    let area = (datosparse[0] * datosparse[1]) / 2;

    const resultado = document.createTextNode(
      `Área del triángulo:${area}`
    );

    node.appendChild(basenode);
    node.appendChild(alturanode);
    node.appendChild(resultado);

    document.getElementById("mostrar").appendChild(node);

    base.value = "";
    altura.value = "";
  } else {
    alert("Complete los valores por favor");
  }
});

let DatosImpar = [];

form2.addEventListener("submit", (e) => {
    e.preventDefault();
  
    if (!(impar.value%2)==0) {
      const node = document.createElement("li");
  
      DatosImpar.push(impar.value);
  
      let datosparse = DatosImpar.splice(",");
  
      const basenode = document.createTextNode(
        "Número impar:" + datosparse[0] + ", "
      );
     
  
      let area  = Math.sqrt(datosparse[0]) ;
  
      const resultado = document.createTextNode(
        `Raíz cuadrada:${area.toFixed(2)}`
      );
  
      node.appendChild(basenode);
      node.appendChild(resultado);
  
      document.getElementById("mostrar-2").appendChild(node);
  
      impar.value = "";

    } else {
      alert("Complete el valor impar o verifique que el número sea impar");
    }
  });




form3.addEventListener("submit", (e) => {
    e.preventDefault();
  
    if (texto.value) {
      const node = document.createElement("li");
      
      let cadenaTexto=[]
      
      cadenaTexto.push(texto.value);
  
      let datosparse = cadenaTexto;



      let unir =  datosparse.toString().split(' ')
    
      let totalcadena =unir.join('')
      
      console.log(totalcadena)
  
      const basenode = document.createTextNode(
        "Cadena de texto:" + datosparse[0] 
      );
     

        
      const resultado = document.createTextNode(
        `Cantidad de longitud de texto: ${totalcadena.length}`
      );
  
      node.appendChild(basenode);
      node.appendChild(resultado);
  
      document.getElementById("mostrar-3").appendChild(node);
  
      texto.value = "";

    } else {
      alert("Complete el valor impar o verifique que el número sea impar");
    }
  });