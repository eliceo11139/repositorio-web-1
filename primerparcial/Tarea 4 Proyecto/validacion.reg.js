
const formulario = document.getElementById("form-reg");

const usuario = document.getElementById("usuario-reg")
const correo = document.getElementById("correo-reg")
const password = document.getElementById("password-reg")
const passwordconf = document.getElementById("password-conf-reg")



const errorUser = document.getElementById("errorUser");
const correoUser = document.getElementById("correoReg");
const passwordReg = document.getElementById("passwordReg");
const passwordconfReg = document.getElementById("passwordconf");
const contranocoincide = document.getElementById("no-password");

const regUserNAme = /^[A-Za-zÑñÁáÉéÍíÓóÚúÜü\s]+$/;
const regCorreo =/^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;  


formulario.addEventListener("submit", (e) =>{

    e.preventDefault();

    if(!regUserNAme.test(usuario.value)){
        errorUser.innerHTML += `<p> Formato de usuario no válido</p>`
        return;
    }  else{
        errorUser.innerHTML += `<p></p>`
    }
    
    if(!regCorreo.test(correo.value)){
        correoUser.innerHTML += `<p> Formato de correo no válido</p>`
        return;
    }  else{
        correoUser.innerHTML += `<p></p>`
    }

    if(password.value===''){
        passwordReg.innerHTML += `<p>Campo de contraseña vacía</p>`
        return;
    }  else{
        passwordReg.innerHTML += `<p></p>`
    }

    if(passwordconf.value===''){
        passwordconfReg.innerHTML += `<p>Campo de confirmar contraseña vacía </p>`
        return;
    }  else{
        passwordconfReg.innerHTML += `<p></p>`
    }
    

    if(password.value !== passwordconf.value){
        contranocoincide.innerHTML += `<p>No coinciden las contraseñas </p>`
    } else{
        contranocoincide.innerHTML+= `<p></p>`
    }

    if(contraseña.value.length<8  ){
        contranocoincide.innerHTML += `<p>Logintud contraseña no válida</p>`

        return;
    }else {
        contranocoincide.innerHTML += `<p></p>`
    }

    setTimeout(() => {
        location="./login.html"
    }, 1500);
 
    
    

} )


