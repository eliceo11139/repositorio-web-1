const firebaseConfig = {
    apiKey: "AIzaSyCplEeuDcKkD4av1rNcJFCNxA72xYS1KjE",
    authDomain: "login-3c2da.firebaseapp.com",
    projectId: "login-3c2da",
    storageBucket: "login-3c2da.appspot.com",
    messagingSenderId: "298088112908",
    appId: "1:298088112908:web:b51ff167ff84185c165f29"
  };
  
  // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    const db = firebase.firestore();
    const saveUsers = ( user )=>{
    db.collection("Usuarios-registrados").add({
        user
    })
    .then((docRef) => {
        console.log("Document written with ID: ", docRef.id);
    })
    .catch((error) => {
        console.error("Error adding document: ", error);
    });

}

$("#boton").on('click', ()=>{
    let correo = $("#correo").val();
    let usuario =$('#usuario').val();
    let password = $("#password").val();
    let validarRegister = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i; 


    //validar correo
    if(correo ===''){
        return alert('El correo esta vacio!');
    }
    else if(!validarRegister.test(correo)){
        return alert('El correo que ingreso no es valido!')
    }

    //Validar Usuario
    if(usuario === ''){
        return alert('El usuario esta vacio!');
    }
    else if(usuario.length < 5){
        return alert('El usuario debe tener mas de 7 caracteres')
    }

    //Validacciones de la contraseña
    if(password ===''){
        return alert('La contraseña esta vacia!')
    }
    else if(password.length < 7){
        return alert('La contraseña debe tener como minio 7 digitos')
    }
    const user={
        correo,
        password,
        usuario
    }
    saveUsers(user);
    let correoLimpiar = document.getElementById("correo");
    let usuarioLimpiar = document.getElementById("usuario");
    let passwordLimpiar = document.getElementById("password");
    correoLimpiar.value = "";
    usuarioLimpiar.value="";
    passwordLimpiar.value="";

})