
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from 'firebase/firestore/lite';

const firebaseConfig = {
  apiKey: "AIzaSyBgOZpwM4biSn5n63Q2yXrkA4uWWEMuYVc",
  authDomain: "proyectof-b0c45.firebaseapp.com",
  projectId: "proyectof-b0c45",
  storageBucket: "proyectof-b0c45.appspot.com",
  messagingSenderId: "135839700081",
  appId: "1:135839700081:web:22c9255178ec6ecb1cf7d0"
};

initializeApp(firebaseConfig);
const auth = getAuth();
const db = getFirestore();

export {auth, db};