import {defineStore} from 'pinia';
import { collection, query, where, getDocs, addDoc, doc, deleteDoc, getDoc, updateDoc } from "firebase/firestore/lite";
import {db} from '../firebaseConfig'
import { auth } from '../firebaseConfig'
import {nanoid } from 'nanoid'
import router from '../router/index';

export const useDatabaseStore = defineStore('database', {
        state: ()=> ({
            documents: [],
            loagingDoc: false
        }),
        actions: {
            async getProyectos(){
                if(this.documents.length !== 0){
                    return;
                }
                this.loagingDoc = true
                try {
                    const q = query(collection(db, "proyectos"), where("user", "==", auth.currentUser.uid))
                    const querySnapshot = await getDocs(q);
                    querySnapshot.forEach((doc) => {
                    this.documents.push(
                        {id: doc.id,
                         ...doc.data() }
                        )
                      });
                } catch (error) {
                    console.log(error)
                } finally {
                    this.loagingDoc = false
                }
            },
            async addProyectos(title,facultad,lider,descripcion,image){
                try {
                    const objetoDoc = {
                        title:title,
                        facultad:facultad,
                        lider:lider,
                        descripcion:descripcion,
                        image:image,
                        short:nanoid(6),
                        user:auth.currentUser.uid
                    }
                    const docRef = await addDoc(collection(db, "proyectos"), objetoDoc);
                    //console.log(docRef);
                    this.documents.push({
                        ...objetoDoc,
                        id: docRef.id
                    })
                } catch (error) {
                    console.log(error)
                } finally{

                }
            },
            async leerProyecto(id){
                try {
                    const docRef = doc(db, 'proyectos', id);
                    
                    const docSnap = await getDoc(docRef);

                    if(!docSnap.exists()){
                        throw new Error("no existe el doc")
                    }
                    if(docSnap.data().user !== auth.currentUser.uid){
                        throw new Error("no le pertenece este documento")
                    }

                    return[docSnap.data().title, docSnap.data().facultad,
                      docSnap.data().lider,
                        docSnap.data().descripcion,
                      docSnap.data().image]
                } catch (error) {
                    console.log(error)
                } finally{

                }
            },
            async updateProyecto(id,title,facultad,lider,descripcion,image ){
                try {
                    const docRef = doc(db, 'proyectos', id);

                    const docSnap = await getDoc(docRef)
                    if(!docSnap.exists()){
                        throw new Error("no existe el doc")
                    }
                    if(docSnap.data().user !== auth.currentUser.uid){
                        throw new Error("no le pertenece este documento")
                    }
                    await updateDoc(docRef, {
                        title:title,
                        facultad:facultad,
                        lider:lider,
                        descripcion:descripcion,
                        image:image
                    })
                    
                    this.documents = this.documents.map(item => item.id===id ? ({...item, title:title, facultad:facultad, lider:lider,descripcion:descripcion,image:image}): item);
                    router.push('/misproyectos')
                } catch (error) {
                    console.log(error)
                } finally{

                }
            },
            async deleteProyecto(id){
                try {
                    const docRef = doc(db, 'proyectos', id);

                    const docSnap = await getDoc(docRef)
                    if(!docSnap.exists()){
                        throw new Error("no existe el doc")
                    }
                    if(docSnap.data().user !== auth.currentUser.uid){
                        throw new Error("no le pertenece este documento")
                    }
                    await deleteDoc(docRef);
                    this.documents = this.documents.filter((item) => item.id !== id)
                } catch (error) {
                    console.log(error)
                } finally{

                }
            }
        }
});